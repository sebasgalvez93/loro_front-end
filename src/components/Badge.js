import React from 'react';

import './styles/Badge.css';
import logo from '../images/logo.svg';
import icon from '../images/logo-loro.svg';

class Badge extends React.Component {
  render() {
    return (
      <div className="Badge">
        <div className="Badge__header">          
          <img src={logo} alt="Logo de loro" />
        </div>

        <div className="Badge__section-name">
          <img src={icon} alt="Logo de la conferencia" />          
          <h1>
            {this.props.firstName} <br /> {this.props.lastName}
          </h1>
        </div>              
      </div>
    );
  }
}

export default Badge;
