import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './styles/Home.css';
import iconLoroImage from '../images/logo-loro.svg';

export default class Home extends Component {
  render() {
    return (
      <div className="Home">
        <div className="container">
          <div className="row">            
            <div className="Home__col d-none d-md-block col-md-6">
              <img
                src={iconLoroImage}
                alt="icono Loro"
                className="img-fluid p-4"
              />
            </div>
            <div className="Home__col col-12 col-md-4">            
              <h1>Sistema de gestión de inventario para Loro</h1>
              <Link className="btn btn-primary" to="/badges">
                Iniciar
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
