# prueba_front-end



Para clonar el proyecto deben abrir la terminal y poner el siguiente texto de Git

$ git clone https://sebasgalvez93@bitbucket.org/sebasgalvez93/loro_front-end.git

Después acceder a la carpeta

$ cd loro_front-end

Ahora se instala npm

$ npm install

Por último para compilar el proyecto se ejecuta

$ npm run start

Con esto la Single-Page-Application ya debe correr de forma automática



#Componentes

Se divide en componentes, paginas, data, imágenes y estilos(CSS) el proyecto para tener una buena práctica de programación y siguiendo los lineamientos de reactjs,

Se hace uso de la herramientas del DOM y JSX para tener puntos de ejecución claros y saber en dónde está fallando la aplicación, 

Se usa React Router para tener una mayor entendibilidad de la app,

Se aplica el uso del uso de vida de los componente para mejor optimización y carga de los componentes,

Navbar.js -> Componente encargado de pintar en todas las páginas de la aplicación el menú,

BadgeForm.js -> Componente para el formulario de nueva sucursal y se puede replicar a las demás entradas,

Branch.js -> Componente para mostrar las sucursales,

Product.js -> Componente para visualizar los productos que están en las sucursales,

#Páginas

Las páginas están compuestas por uno o varios de los componentes descritos en la parte de arriba,

#Index.js y global.css

Página de inicio de la app y el estilo general que aplica a uno o varios elementos

#Características de un buen código

Este debe ser bien estructurado para que otro desarrollador con el conocimiento necesario pueda hacer seguimiento y agregar funcionalidad sin que afecte al ya creado


